import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'


export default defineConfig({
  plugins: [vue()],
  template: {
    compilerOptions: {
      isCustomElement: (tag) => tag.includes('v-'),
    }
  },
  test: {
    globals: true,
    environment: 'jsdom',
    setupFiles: './test/setupTests.ts',
    deps: {
      inline: ["vuetify"],
  }
  }
})

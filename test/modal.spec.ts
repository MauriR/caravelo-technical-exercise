import userEvent from "@testing-library/user-event"
import { fireEvent, render, RenderResult, screen } from "@testing-library/vue"
import Modal from "../src/components/Modal/Modal.vue"


describe("Modal", () => {
  it("renders the modal with the given props", () => {
    SUT.render()
  })

  it("Disables add quota button when maximum flights number is reached", async () => {
    SUT.render()

    await SUT.reachMaximumQuota()

    expect(await SUT.addButton()).toHaveAttribute("disabled", "true")
  })

  it("Disables remove quota button when minimum flights number is reached", async () => {
    SUT.render()

    await SUT.reachMinimumQuota()
    screen.debug()

    expect(await SUT.removeButton()).toHaveAttribute("disabled", "true")
  })

  it("disables selector if there was no quota change", async () => {

    SUT.render()

    expect(await SUT.reasonSelector()).toHaveAttribute("disabled", "true")
  })

  it("does not show any option in the selector if there was no quota change", () => {
    SUT.render()

    const initialReasons = SUT.reasons().length

    expect(initialReasons).toBe(0)
  })

  it("shows options in the selector after quota change", async () => {
    SUT.render()

    await SUT.reachMaximumQuota()

    const optionsAfterQuotaIncrease:string = "Subscriber canceled flight,Airline canceled flight,Customer Compensation,Other"
    expect(await SUT.reasonSelector()).toHaveAttribute("items", optionsAfterQuotaIncrease)
  })

  it.skip("enables Save Changes button only if there was a change of quota and a reason was selected", async () => {
    SUT.render()

    expect(await SUT.saveButton()).toHaveAttribute("disabled", "true")

    await SUT.reachMaximumQuota()
    await SUT.selectReason()

    screen.debug()

    expect(await SUT.saveButton()).toHaveAttribute("disabled", "false")
  })
})

class SUT {
  static render(): RenderResult {
    return render(Modal, {
      props: {
        dialog: true,
        details: {
          name: "Airline",
          corporateColors: ["white", "black"],
          flightsLeft: 1
        }
      },
    })
  }

  static async reachMaximumQuota(): Promise<void> {
    await SUT.addQuota()
    await SUT.addQuota()
    await SUT.addQuota()
  }

  static async reachMinimumQuota(): Promise<void> {
    await SUT.removeQuota()
    await SUT.removeQuota()

  }

  static async addQuota(): Promise<void> {
    await userEvent.click(await SUT.addButton())
  }

  static async removeQuota(): Promise<void> {
    await userEvent.click(await SUT.removeButton())
  }

  static async selectReason(): Promise<void> {
    await fireEvent.update(await SUT.reasonSelector(), "Other")
  }

  static async addButton(): Promise<HTMLElement> {
    return await screen.findByText("+")
  }

  static async removeButton(): Promise<HTMLElement> {
    return await screen.findByText("-")
  }

  static async saveButton(): Promise<HTMLElement> {
    return await screen.findByText("Save Changes")
  }

  static async reasonSelector(): Promise<HTMLElement> {
    return await screen.findByTestId("selector")
  }

  static reasons(): HTMLElement[] {
    return screen.queryAllByRole("option")
  }
}
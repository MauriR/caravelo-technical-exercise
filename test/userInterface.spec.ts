import { render, screen, waitFor } from "@testing-library/vue"
import userEvent from "@testing-library/user-event"
import UserInterface from "../src/views/UserInterface.vue"

describe("User Interface", () => {
  it("renders airlines cards", async () => {

    SUT.render()

    expect(await SUT.airlineCard()).toBeInTheDocument()
  })

  it("opens modal after selecting the desired airline", async () => {
    SUT.render()

    await SUT.selectAirline()

    waitFor(async () => {
      expect(await SUT.modalHeader()).toBeInTheDocument()
    })
  })
})

class SUT {
  static render() {
    render(UserInterface)
  }

  static async airlineCard(): Promise<HTMLElement> {
      const name = "Arctic Airlines"
      return await screen.findByText(name)
    }

  static async modalHeader() {
      const title = "Edit Flights"
      return await screen.findAllByText(title)[0]
    }

  static async selectAirline() {
      const airline = await screen.findAllByRole("button")[0]
      
     await  userEvent.click(airline)
    }

  }
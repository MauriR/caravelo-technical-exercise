-Español
-English

## Español
Esta aplicación corresponde al test técnico de la empresa Caravelo. 
La app está hecha con Vue 3 (su modalidad de Script Setup) y testeada con Testing Library y Vitest.

### Setup del Proyecto
Para poder lanzar la aplicación es necesario instalarse las dependencias necesarias. Para ello ejecutaremos el comando `npm install` en nuestra terminal.

### Compilación
Una vez tengas todos los paquetes necesarios, puedes montar la aplicación de forma local lanzando el siguiente comando: `npm run dev`
Al finalizar el proceso, en consola aparecerá el puerto en el que se ha montado la aplicación. Clickando sobre la dirección mientras mantienes mayúscula apretada, accederás de forma inmediata.

### Posibles errores al compilar

Si aparece el siguiente error: 
```
await import('source-map-support').then((r)=>r.defaut.install())
SyntaxError: Unexpected reserved word
```
o

```
import { performance } from 'node:perf_hooks'
```
estaremos ante un problema con las versiones de Node. Ejectua el comando `node -v` ( o `nvm list` si tienes *Node Version Manager* instalado) y asegúrate que estás usando una versión reciente (lo recomendable sería la 18). Puedes cambiar de versión usando (en este caso)`nvm use 18.16.0` o una más reciente que la que estamos usando y tengamos instalada. 

### Backend
Al ser una prueba para una posición de Frontend, la app se vale del archivo *airlines.ts* que hace las veces de base de datos falsa. Todos los cambios que se realicen en dicho archivo se perderán al recargar la página. 
El código de la app contiene una carpeta servicios para simular mejor la llamada a *airlines.ts* como si éste fuese una base de datos real. 

### Testing
Esta aplicación usa Testing Library y Vitest para los test unitarios. 
Para lanzar los se deberá usar el comando `npm run test`.

Al carecer de Backend la aplicación, no hay test *e2e*. 


## English
*Disclaimer: This guide has been written by a non native english speaker and may contain grammar mistakes. 

This app corresponds to Caravelo's technical test. 
It is made with Vue 3 (its Script setup variant) and tested with Testing Library and Vitest. 

### Project Setup

In order to run the application it is necessary to install the required dependencies. To that purpose command `npm install` must be run in your terminal.  

### Compilation

Once you have all the necessary packages, you can locally build the app  by typing command `npm run dev`.
After the process is completed, you will see the port number where the app is mounted in console.

### Possible errors when compiling

If one the following errors appears: 
```
await import('source-map-support').then((r)=>r.defaut.install())
SyntaxError: Unexpected reserved word
```
or

```
import { performance } from 'node:perf_hooks'
```

that will mean that we are facing a problem with Node versions. Run the comand  `node -v` ( or `nvm list` if you have *Node Version Manager* installed) and make sure you are using a recen version (the optimal would be version 18). Node version can be changed by using (in our case) `nvm use 18.16.0` or one more recent version than the one we are using and that we have installed.

### Backend
Since this is a test for a Frontend position, the app uses the file *airlines.ts* as a fake data base. All the changes made in this file will be lost after reloading the webpage. 
The app code contains a services folder to simulate better the call to *airlines.ts* as if this file were a real data base. 

### Testing
This aplication uses Testing Library and Vitest for its unit tests. 
In order to throw the test, the command `npm run test` must be used. 

Since the app has no Backend, there are no *e2e* test. 

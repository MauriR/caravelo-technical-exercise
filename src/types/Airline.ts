export type Airline = {
  name: string
  id: string
  corporateColors: string[]
  logo: string
  flightsLeft:number
  historicChanges:string[]
}

export const emptyAirline = {
  name: "",
  id: "",
  corporateColors: [],
  logo:"",
  flightsLeft:0,
  historicChanges:[]
}
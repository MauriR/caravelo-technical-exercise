import { Airline } from "./types/Airline";

export const airlines:Airline[] = [
  {
    name: "Arctic Airlines",
    id: "1",
    corporateColors: ["#24b4b4", "#ffffff"],
    logo: "./logos/arcticAirlines.png",
    flightsLeft: 2,
    historicChanges: []
  },
  {
    name: "Air Mauri",
    id: "2",
    corporateColors: ["#8c6f1b", "#131862"],
    logo: "./logos/airMauri.png",
    flightsLeft: 1,
    historicChanges: []
  }
]
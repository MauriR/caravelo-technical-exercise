export const reasons:Record<string, string[]> =
{
  afterAdding: [
    "Subscriber canceled flight",
    "Airline canceled flight",
    "Customer Compensation",
    "Other"
  ],

  afterRemoving: [
    "Flight not redeposited after a flight cancellation",
    "Subscriber had log in or password issues when booking",
    "Subscription has not renewed correctly",
    "Other"
  ],

  ifNoChanges: []
}


import { airlines } from "../airlines";
import { Airline } from "../types/Airline";

export default class Airlines {
  async obtainAirlines(): Promise<Airline[]> {
    return airlines
  }

  async updateSubscriber(flights: number, changeReason: string, airlineName: string) {
    const index = airlines.findIndex((airline) => airline.name === airlineName)

    airlines[index].flightsLeft = flights
    airlines[index].historicChanges.push(changeReason)
  }
}